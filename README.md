# Yogi Engineer Technical Assessment

This is an assignment given by Yogi as a technical assessment during their interviewing process.

## Goal

The provided instructions can be seen in the [Instructions](./Instructions/) directory.  The following is a summarized list of the project requirements:

1. Stand up a Postgres database containing the provided data.
1. The unique values of “brand name”.
1. The unique values of “product name”.
1. The count of reviews.
1. The count of views.
1. The average rating for each value of “brand name”.
1. The average rating for each value of “product name”.

---

## Assumptions Made

1. I assumed that while a backup was provided with CSVs for reference, that I was allowed to add additional objects to the database schema and/or change the tables themselves.  There was nothing forbidding this in the instructions, and it made it easier to develop the API.
1. The instructions specified "When designing your algorithms, please keep scalability in mind (pretend thousands of reviews) and what’s the best schema for a frontend to use.".  By this I assumed the end resulting JSON could be in a schema I choose, rather than being an exact copy of the reference schema provided.
1. Product and brand-level data was desired based on the provided [schema](./Instructions/Schema.json).
1. I assumed that since a database was intended to be used that the API was also intended to connect to this database rather than simply using the CSVs in memory.

---

## End Result

### Database

I stood up a custom Postgres database using a [docker-compose.yml](./docker-compose.yml) file.  All of the files for the Postgres DB are in [SQL](/SQL/). Since I assumed I could add DB objects, I created functions on the database to handle getting the desired data, and the Python endpoints ended up just calling the DB rather than doing any heavy processing.  The data schema of the endpoints is actually determine entirely based on what the functions return.  This is not applicable in all scenarios, but I did this to allow the data to be much more dynamic and flexible.  If we add calculations to the functions in the database, no redeployment of the Python code is necessary.

By offloading all of the heavy processing to the database, we are able to handle larger masses of request and process larger datasets more quickly.  SQL, as a query language, is designed to do this sort of data fetching en masse.  Thus, our scalability is made much much easier since we are using a tool inherently designed to run fast at large scale.

### Python

I created a Python project using Flask and Flask SQL Alchemy to handle the web server and database communication respectively.

I created _two_ endpoints instead of just one, as I decided it would be the best way to send the desired data up to the frontend with the best schema.  My reasoning for this is that both product _and_ brand level data was desired, and well-designed REST API endpoints should return a single dataset or do a single job.  By combining the two, we were mixing results together, and if the frontend only wanted brand data, they would need to parse through unnecessary product data to get to it.  Even if the schema was created so as to separate the two datasets, sending back more data than the frontend needs increases network latency for every request and reduces scalability.

The resulting web server is very lean as most of the processing logic happens in the database. Since Python, as a language, is very slow compared to an SQL query language, we are able to gain a large speed increase at scale.  Python also doesn't support multi-threaded and parallel requests very well, so decreasing the amount of work we have to do in Python will help to alleviate network latency at high request volumes.

---

## Running Application

### Postgres

1. If you have Docker installed, you can run `docker-compose up` to start up the Postgres SQL database.  The volume paths may need to be changed, as the assumption was made it would be run on a Linux machine.
1. Once it's running, the tables and functions in the [SQL](/SQL/) folder should be created on the database.
1. Insert your data into the various tables.

### Flask

1. Install dependencies via `pip install -r requirements.txt`.
1. Add the following environment variables based on your database setup:
    1. `POSTGRES_URL` -> The URL to the hosted Postgres server.  By default this is `localhost:5432`.
    1. `POSTGRES_USER` -> The user in the Postgres database to use for access.  By default this is `postgres`.
    1. `POSTGRES_PW` -> The password for the specified user.  By default this is `postgres`.
    1. `POSTGRES_DB` -> The database within the Postgres instance to use.  By default this is `postgres`.
1. Run `python yogi.py`.  A Flask web server will be started up.
1. Navigate to the URL and call the endpoints.  By default, the web server is hosted at `localhost:5000`.

---

## Schema

Since the Python code simply parses and returns the Postgres columns based on the functions called in the database, this schema is dependent on the returns columns and their names.

### /Reviews/ProductReviews

Returns a list of review metrics per product, across all reviews for that product:

```json
[
    {
        "avg_rating": double,
        "brand_id": int,
        "brand_name": str,
        "product_id": int,
        "product_name": str,
        "review_count": int,
        "views": int
    },
    ...
]
```

Example:

```json
[
    {
        "avg_rating": 4.5,
        "brand_id": 1,
        "brand_name": "Hisense",
        "product_id": 1,
        "product_name": "Hisense U6G Series 4K ULED Android Smart TV",
        "review_count": 2,
        "views": 100
    },
    {
        "avg_rating": 1.0,
        "brand_id": 1,
        "brand_name": "Hisense",
        "product_id": 2,
        "product_name": "Hisense U7G Series 4K ULED Android Smart TV",
        "review_count": 1,
        "views": 150
    },
    {
        "avg_rating": 3.0,
        "brand_id": 2,
        "brand_name": "Samsung",
        "product_id": 3,
        "product_name": "Samsung Q60A Series QLED 4K UHD Smart Tizen TV",
        "review_count": 3,
        "views": 375
    },
    {
        "avg_rating": 4.0,
        "brand_id": 3,
        "brand_name": "Vizio",
        "product_id": 5,
        "product_name": "Vizio P-Series Quantum LED 4K HDR Smart TV",
        "review_count": 2,
        "views": null
    }
]
```



### /Reviews/BrandReviews

Returns a list of review metrics per brand, across all reviews for all products for that brand:

```json
[
    {
        "avg_rating": decimal,
        "brand_id": int,
        "brand_name": str,
        "review_count": int,
        "views": int
    },
    ...
]
```

Example:

```json
[
    {
        "avg_rating":3.3333333333333335,
        "brand_id":1,
        "brand_name":"Hisense",
        "review_count":3,
        "views":350
    },
    {
        "avg_rating":3.0,
        "brand_id":2,
        "brand_name":"Samsung",
        "review_count":3,
        "views":1125
    },
    {
        "avg_rating":4.0,
        "brand_id":3,
        "brand_name":"Vizio",
        "review_count":2,
        "views":null
    }
]
```
