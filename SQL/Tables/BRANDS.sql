-- Enable use of the case-insentive text column.
CREATE EXTENSION IF NOT EXISTS CITEXT;

/**
 * Stores brand names for the various products.
 */
CREATE TABLE BRANDS
(
    -- The unique brand ID. We can use an IDENTITY column to generate new IDs for us automatically.
    ID INT NOT NULL GENERATED ALWAYS AS IDENTITY
        -- Since this is unique across all brands, make it our primary key.
        CONSTRAINT PK_BRANDS PRIMARY KEY,
    -- The name of the brand.  200 was choosen arbitraily, and can be increased or decreased as
    -- needed.  Avoid duplicate brand names by enforcing column uniqueness.  Make this case
    -- insenstive so "Foobar" and "foobar" cannot both be added.
    NAME CITEXT NOT NULL UNIQUE
)
