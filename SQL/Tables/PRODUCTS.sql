-- Enable use of the case-insentive text column.
CREATE EXTENSION IF NOT EXISTS CITEXT;

/**
 * Stores products and product view counts associated with their brand.
 */
CREATE TABLE PRODUCTS
(
    -- The unique product ID. We can use an IDENTITY column to generate new IDs for us
    -- automatically.
    ID INT NOT NULL GENERATED ALWAYS AS IDENTITY
        -- Since this is unique across all products, make it our primary key.
        CONSTRAINT PK_PRODUCTS PRIMARY KEY,
    -- The name of the product.  200 was choosen arbitraily, and can be increased or decreased as
    -- needed.  Avoid duplicate product names by enforcing column uniqueness.  Make this case
    -- insenstive so "Foobar" and "foobar" cannot both be added.
    NAME CITEXT NOT NULL UNIQUE,
    -- The number of times this product has been viewed by customers on a product website.
    -- This can be NULL if the data is unavialable or the product has not been viewed.
    VIEWS INT NULL,
    -- The brand this product is for.
    BRAND_ID INT NOT NULL
        -- Ensure a product cannot be added for a brand that doesn't exist.
        CONSTRAINT FK_PRODUCTS_BRAND_ID REFERENCES BRANDS (ID)
        -- If the brand is deleted, automatically delete all products associated with it.
        -- This might not be wanted if you desire errors when you attempt to remove abrand 
        -- with products.  However, cascades can be very helpful in avoding orphaned data.
        ON DELETE CASCADE
)
