/**
 * Stores customer reviews for a specific product. 
 */
CREATE TABLE REVIEWS 
(
    -- The unique review ID. We can use an IDENTITY column to generate new IDs for us automatically.
    ID INT NOT NULL GENERATED ALWAYS AS IDENTITY
        -- Since this is unique across all reviews, make it our primary key.
        CONSTRAINT PK_REVIEWS PRIMARY KEY,
    -- The review text body.  This can be NULL if someone simply provide a numerical rating and no
    -- text.
    BODY TEXT NULL,
    -- The numerical rating of the review on a 1 to 5 start scale.
    RATING INT NOT NULL,
        -- Assuming we are operating on a 1 to 5 star rating system, constraints the input so we
        -- don't have invalid rating values.
        CONSTRAINT CH_REVIEWS_RATING CHECK (RATING >= 1 AND RATING <= 5),
    -- The brand this product is for.
    PRODUCT_ID INT NOT NULL
        -- Ensure a review cannot be added for a product that doesn't exist.
        CONSTRAINT FK_REVIEWS_PRODUCT_ID REFERENCES PRODUCTS (ID)
        -- If the product is deleted, automatically delete all reviews associated with it.
        -- This might not be wanted if you desire errors when you attempt to remove a product
        -- with reviews.  However, cascades can be very helpful in avoding orphaned data.
        ON DELETE CASCADE
)
