/**
 * Gets brand review metrics for a distinct list of brands.
 */
CREATE FUNCTION get_brand_review_metrics()
    RETURNS TABLE
    (
        BRAND_ID INT,
        BRAND_NAME CITEXT,
        VIEWS INT,
        REVIEW_COUNT INT,
        AVG_RATING FLOAT
    )
    LANGUAGE sql
AS
$$
    -- SELECT DISTINCT here so if we have multiple reviews for the same brand, we only get one
    -- brand.  The NAME column for the BRANDS tables ensure we never get duplicate names, so we just
    -- need the DISTINCT because we join against the REVIEWS table.
    SELECT DISTINCT
        BRAND.ID AS BRAND_ID,
        BRAND.NAME AS BRAND_NAME,
        -- Get the number of total brand views across all products for the same brand.
        SUM(PRODUCT.VIEWS) OVER(PARTITION BY PRODUCT.BRAND_ID) AS VIEWS,
        -- Get the number of ratings for a particular brand by counting the unique IDs tied to the
        -- same BRAND_ID.
        COUNT(REVIEW.ID) OVER(PARTITION BY PRODUCT.BRAND_ID) AS REVIEW_COUNT,
        -- Average each brand's rating separately by partiting the result set by the BRAND_ID.
        AVG(REVIEW.RATING) OVER(PARTITION BY PRODUCT.BRAND_ID) AS AVG_RATING
    FROM REVIEWS REVIEW
    -- Match each review with it's product.
    INNER JOIN PRODUCTS PRODUCT
        ON PRODUCT.ID = REVIEW.PRODUCT_ID
    -- Match each product with it's brand.
    INNER JOIN BRANDS BRAND
        ON BRAND.ID = PRODUCT.BRAND_ID
    -- Order the results by the brand's ID so we can deterministically get the same order
    -- everytime.  I know it would probably do this under the hood, but I like to be explicit.
    ORDER BY BRAND.ID;
$$
