/**
 * Gets product review metrics for a distinct list of products.
 */
CREATE FUNCTION get_product_review_metrics()
    RETURNS TABLE
    (
        BRAND_ID INT,
        PRODUCT_ID INT,
        BRAND_NAME CITEXT,
        PRODUCT_NAME CITEXT,
        VIEWS INT,
        REVIEW_COUNT INT,
        AVG_RATING FLOAT
    )
    LANGUAGE sql
AS
$$
    -- SELECT DISTINCT here so if we have multiple reviews for the same product, we only get one
    -- product.  The NAME columns for the PRODUCTS and BRANDS tables ensure we never get
    -- duplicate names, so we just need the DISTINCT because we join against the REVIEWS table.
    SELECT DISTINCT
        BRAND.ID AS BRAND_ID,
        PRODUCT.ID AS PRODUCT_ID,
        BRAND.NAME AS BRAND_NAME,
        PRODUCT.NAME AS PRODUCT_NAME,
        -- The product views are just the raw column value.
        PRODUCT.VIEWS,
        -- Get the number of ratings for a particular product by counting the unique IDs tied to
        -- the same PRODUCT_ID.
        COUNT(REVIEW.ID) OVER(PARTITION BY REVIEW.PRODUCT_ID) AS REVIEW_COUNT,
        -- Average each produdct's rating separately by partiting the result set by the
        -- PRODUCT_ID.
        AVG(REVIEW.RATING) OVER(PARTITION BY REVIEW.PRODUCT_ID) AS AVG_RATING
    FROM REVIEWS REVIEW
    -- Match each review with it's product.
    INNER JOIN PRODUCTS PRODUCT
        ON PRODUCT.ID = REVIEW.PRODUCT_ID
    -- Match each product with it's brand.
    INNER JOIN BRANDS BRAND
        ON BRAND.ID = PRODUCT.BRAND_ID
    -- Order the results by the product's ID so we can deterministically get the same order
    -- everytime.  I know it would probably do this under the hood, but I like to be explicit.
    ORDER BY PRODUCT.ID;
$$
