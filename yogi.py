# Python built-ins.
import argparse
import pdb
import sys
import os

# Third-party imports.
from flask import Flask, Response
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy


# This needs to be set globally so all of our various functions can access it.
APP = Flask(__name__)


def get_env_variable(name: str) -> str:
    """Given an environment variable name, attempts to get the value for this environment variable.
    If it doesn't exist, it throws an error.

    Args:
        name (str): The environment variable to get.

    Raises:
        Exception: If the environment variable could not be found.

    Returns:
        str: The value of the environment variable.
    """
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)


# Get the environment variables for each of the values our Postgres connection needs.
POSTGRES_URL = get_env_variable("POSTGRES_URL")
POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PW = get_env_variable("POSTGRES_PW")
POSTGRES_DB = get_env_variable("POSTGRES_DB")

# Setup our Postgres connection string.
DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(
    user=POSTGRES_USER,
    pw=POSTGRES_PW,
    url=POSTGRES_URL,
    db=POSTGRES_DB
)

# Update the Flask app config to include SQLAlchemy specific data.
APP.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
# This setting silences a deprecation warning.
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Wrap our SQL connection around our Flask app so it attaches to the endpoints.
DB = SQLAlchemy(APP)


def post_mortem_exception_handler(exception_type, exception,
                                  traceback, except_hook=sys.excepthook) -> None:
    """Starts a post-mortem debug shell at the point of failure.

    This processes exceptions normally in all other ways.

    Args:
        exception_type (Type[BaseException]): Exception type being raised.
        exception (BaseException): Exception instance being raised.
        traceback (TracebackType): Traceback of the exception which is used to open a debug shell.

    KArgs:
        except_hook (Function, optional): Exception handler to add debugging to.
        Defaults to sys.excepthook.
    """
    except_hook(exception_type, exception, traceback)
    pdb.post_mortem(traceback)


def main() -> int:
    """The entry point into the application which handles argument parsing and running the
    appropriate logic.

    Returns:
        int: The status code of the application to be returned to the system.
    """
    parser = argparse.ArgumentParser(description="Yogi technical assessment API.")
    parser.add_argument("-d", "--debug", required=False, action="store_true",
                        help="Enables a PDB post-mortem traceback for diagnosing and debugging " +
                        "errors.")

    # Parse the arguments into argparse's namespace, and then turn them into a dictionary.
    # This way, we can pop arguments off and pass around the whole set to classes via *args or
    # **kargs if need be.
    args_dict = vars(parser.parse_args())

    # If desired, set the system's exception handler to the post-mortem handler for easier
    # debugging.
    if args_dict["debug"]:
        sys.excepthook = post_mortem_exception_handler

    # Run the flask web server.
    APP.run(host="localhost", port=5000)


def get_and_parse_db_results(function_name: str) -> Response:
    """Given a function name, gets data from the database and parses the results into a Flask
    response object.  This assumes the database function accepts no arguments.

    Args:
        functionName (str): The SQL function to call, which accepts no arguments.

    Returns:
        Response: A Flask Response object containing the parsed DB results.
    """
    # Run an SQL query to call our function.
    # This way, we don't have to worry about maintaining the SQL in our codebase.  We can easily
    # make database changes without even needing to redeploy.
    db_results = DB.engine.execute("SELECT * FROM " + function_name + "()")

    columns = []
    parsed_rows = []

    # Get a list of our columns.  This way, we don't even need to know what the SQL function
    # returns.  If we decided we want to add or remove fields, a simple update to the SQL also
    # updates this endpoint's responses.
    for column in db_results.keys():
        columns.append(column)

    # Get the data for each row.
    for row in db_results:
        row_data = {}
        # Now iterate, in order, our columns to dynamically pull the data.
        # We could do row 0, row 1, but this way our field names match our column names.
        for column in columns:
            row_data[column] = row[column]
        # Append the row to our result list.
        parsed_rows.append(row_data)

    # Since our results are a list, use Flask's jsonify function to return a JSON list.
    return jsonify(parsed_rows)


@APP.route("/Reviews/ProductMetrics", methods=["GET"])
def get_reviews_product_metrics() -> Response:
    """Gets review metrics for products, such as the average review rating and the review count.

    Returns:
        Response: A list of review metrics per product as a JSON string.
    """
    return get_and_parse_db_results("get_product_review_metrics")


@APP.route("/Reviews/BrandMetrics", methods=["GET"])
def get_reviews_brand_metrics() -> Response:
    """Gets review metrics for brands, such as the average review rating and the review count.

    Returns:
        Response: A list of review metrics per brand as a JSON string.
    """
    return get_and_parse_db_results("get_brand_review_metrics")


# Only run main if we are executing this file directly.  This prevents the flask web server from
# running if this file gets imported.
if __name__ == "__main__":
    main()
